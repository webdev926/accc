import React, { useState } from "react";
import InputFeild from "./InputFeild";
import NumberList from "./NumberList";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  ruler: {
    margin: "20px 0px",
  },
}));

const HomeView = () => {
  const classes = useStyles();
  // States to get the input Values and List
  const [numbersList, setNumbersList] = useState([]);

  // function to adding the numbers on submit the
  const addNumberToList = (number) => {
    const updatedList = [number].concat(numbersList);
    setNumbersList(updatedList);
  };

  // function to add the reordered list for the next time
  const handleOrderChange = (reorderedList) => {
    setNumbersList(reorderedList);
  };

  return (
    <Container maxWidth="md">
      <Box my={4}>
        <InputFeild handleSubmit={addNumberToList} />
        <Divider className={classes.ruler} />
        <NumberList list={numbersList} onReorder={handleOrderChange} />
      </Box>
    </Container>
  );
};

export default HomeView;
