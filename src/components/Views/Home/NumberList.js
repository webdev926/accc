import React from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DragHandleIcon from "@material-ui/icons/DragHandle";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0px 2px 1px rgba(0,0,0,0.1)",
    marginBottom: "10px",
  },
}));

const NumberList = ({ list, onReorder }) => {
  const classes = useStyles();

  // Function to handle the Reordering of the list
  const handleDragEnd = (result) => {
    if (!result.destination) {
      alert("oops you cannot drop item outside the list");
      onReorder(list);
    } else {
      const items = Array.from(list);
      const [reorderedItem] = items.splice(result.source.index, 1);
      items.splice(result.destination.index, 0, reorderedItem);
      onReorder(items);
    }
  };
  const renderNumbers =
    list.length === 0 ? (
      <h1>No entries</h1>
    ) : (
      list.map((number, index) => {
        return (
          <Draggable key={number} draggableId={number} index={index}>
            {(provided) => (
              <ListItem
                button
                className={classes.root}
                title={" Click and drag to reorder"}
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
              >
                <ListItemIcon>
                  <DragHandleIcon />
                </ListItemIcon>
                <ListItemText primary={number} />
              </ListItem>
            )}
          </Draggable>
        );
      })
    );

  return (
    <Box>
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable droppableId="numbers">
          {(provided) => (
            <List {...provided.droppableProps} ref={provided.innerRef}>
              {renderNumbers}
              {provided.placeholder}
            </List>
          )}
        </Droppable>
      </DragDropContext>
    </Box>
  );
};

NumberList.propTypes = {
  list: PropTypes.array,
  onReorder: PropTypes.func,
};

export default NumberList;
