export const formattedNumber = (value) => {
  const first3Nos = value.substring(0, 3);
  const middle3nos = value.substring(3, 6);
  const last4nos = value.substring(6, 10);

  return `(${first3Nos}) ${middle3nos}-${last4nos}`;
};
