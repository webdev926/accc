import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { formattedNumber } from "./utils";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    width: "50%",
  },
  button: {
    marginLeft: "20px",
  },
  input: {
    border: "1px solid #000",
  },
}));

const InputFeild = ({ handleSubmit }) => {
  const classes = useStyles();

  const [number, setNumber] = useState("");
  const [isError, setIsError] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);

  const handleClick = () => {
    handleSubmit(formattedNumber(number));
    setNumber("");
  };

  const handleChange = (e) => {
    const phNo = e.target.value;
    setNumber(e.target.value);
    if (!new RegExp(/[a-zA-z\W]/).exec(phNo) && phNo.length === 10) {
      setIsDisabled(false);
      setIsError(false);
    } else {
      setIsDisabled(true);
      setIsError(true);
    }
  };

  return (
    <Box className={classes.root}>
      <TextField
        variant="outlined"
        label="Enter the phone number"
        value={number}
        fullWidth
        size="small"
        onChange={handleChange}
        error={isError}
      />
      <Button
        className={classes.button}
        variant="contained"
        color="primary"
        onClick={handleClick}
        disabled={isDisabled}
      >
        Submit
      </Button>
    </Box>
  );
};

InputFeild.propTypes = {
  handleSubmit: PropTypes.func,
};

export default InputFeild;
